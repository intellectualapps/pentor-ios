//
//  UserService.swift
//  PentorApp
//
//  Created by Segun Solaja on 1/22/17.
//  Copyright © 2017 Pentor. All rights reserved.
//

import UIKit
import Alamofire

class UserService: NSObject {
    enum Key: String {
        case loginType = "auth-type"
        case email
        case password
        case message
    }
    
    enum ErrorMessage: String {
        case userAlreadyExists = "User already exists"
    }
    
    enum LoginType: String {
        case email = "email"
        case facebook = "FB"
        case twitter = "TW"
    }


    deinit {
        print(NSStringFromClass(self.classForCoder) + "." + #function)
    }

    static let user: User? = User.archivedUser()
    
    class func signUp(type: LoginType,
                      email:String,
                      password:String,
                      resultCallback: @escaping (User) -> Void, errorCallback: @escaping (_ err:String) -> Void ){
        
        let param = [Key.email.rawValue: email,
                     Key.password.rawValue: password]
        
        Alamofire.request(URLHelper.getURL("SIGN_UP") + "?email=\(email)&password=\(password)",
                          method: .post,
                          parameters: param,
                          encoding: JSONEncoding.default,
                          headers: nil)
            .responseJSON { (result) in
                print(result.result.value ?? "error")
                if(result.result.isSuccess){
                    let result = result.result.value as? Dictionary<String,AnyObject>
                    if let _ = result?["developerMessage"] as? String,
                        let message = result?[Key.message.rawValue] as? String {
                        if message == ErrorMessage.userAlreadyExists.rawValue {
                            if type != .email {
                                login(email: email,
                                      password: password,
                                      type: type,
                                      resultCallback: resultCallback,
                                      errorCallback: errorCallback)
                            } else {
                                errorCallback(message)
                            }
                        } else {
                            errorCallback(message)
                        }
                    } else {
                        let user  = UserConverter.convert(userJson: result)
                        let userData = NSKeyedArchiver.archivedData(withRootObject: user)
                        UserDefaults.standard.set(userData, forKey: "user")
                        resultCallback(user)
                    }
                    //resultCallback(true)
                }else{
                    errorCallback(result.result.error!.localizedDescription)
                    
                    print("error")
                }
        }
        
    }
    
    class func login(email: String,
                     password: String,
                     type: LoginType,
                     resultCallback: @escaping (User) -> Void,
                     errorCallback: @escaping (_ err:String) -> Void) {
        
        let param: [String: Any] = [Key.email.rawValue: email,
                                    Key.password.rawValue: password]
        Alamofire.request(URLHelper.getURL("LOGIN") + "?email=\(email)&password=\(password)",
                          method: .post,
                          parameters: param,
                          encoding: JSONEncoding.prettyPrinted,
                          headers: nil)
            .response(completionHandler: { (response) in
                print(response)
                if let raw = String(data: response.data!, encoding: .ascii) {
                    print(raw)
                }
            })
            .responseString(completionHandler: { (response) in
                print(response)
            })
            .responseJSON { (result) in
                print(result.result.value ?? "error")
                if(result.result.isSuccess){
                    let result = result.result.value as? Dictionary<String,AnyObject>
                    if let _ = result?["developerMessage"] as? String{
                        errorCallback(result!["message"] as! String)
                    }else{
                        let user  = UserConverter.convert(userJson: result!)
                        let userData = NSKeyedArchiver.archivedData(withRootObject: user)
                        UserDefaults.standard.set(userData, forKey: "user")
                        resultCallback(user)
                    }
                    //resultCallback(true)
                }else{
                    errorCallback(result.result.error!.localizedDescription)
                    if let responce = String(data: result.data!, encoding: .utf8) {
                        print(responce)
                    }
                    
                    print("error")
                }
        }
        
    }
    
    
    class func updateProfile(_ bodyParam: [String: Any], resultCallback:@escaping (User) -> Void ,errorCallback:@escaping (_ err:String) -> Void){
        
        Alamofire.request(URLHelper.getURL("UPDATE_PROFILE"), method: .put, parameters: bodyParam, encoding: URLEncoding.default, headers: Constants.headerConstants(["content-type": "application/x-www-form-urlencoded"])).responseJSON { (result) in
            if(result.result.isSuccess){
                let result = result.result.value as? Dictionary<String,AnyObject>
                if let _ = result?["developerMessage"] as? String{
                    errorCallback(result!["message"] as! String)
                }else{
                    let user  = UserConverter.convert(userJson: result!)
                    if let usr = self.user{
                        user.authToken = usr.authToken
                    }
                    let userData = NSKeyedArchiver.archivedData(withRootObject: user)
                    UserDefaults.standard.set(userData, forKey: "user")
                    resultCallback(user)
                }
                //resultCallback(true)
            }else{
                errorCallback(result.result.error!.localizedDescription)
                
                print("error")
            }
            
        }
    }
    
    class func getInterest(resultCallback:@escaping ([Interest]) -> Void ,errorCallback:@escaping (_ err:String) -> Void){
        
        Alamofire.request(URLHelper.getURL("INTEREST"), method: .get, parameters: nil, encoding: URLEncoding.default, headers: Constants.headerConstants([:])).responseJSON { (result) in
            if(result.result.isSuccess){
                let result = result.result.value as? Dictionary<String,AnyObject>
                if let _ = result?["developerMessage"] as? String{
                    errorCallback(result!["message"] as! String)
                }else{
                    let interestJson = result?["interests"] as? [Dictionary<String,AnyObject>]
                    let interests = interestJson?.map({ (json) -> Interest in
                        return UserConverter.convertInterest(interstJson: json)
                    })
                    resultCallback(interests!)
                }
                //resultCallback(true)
            }else{
                errorCallback(result.result.error!.localizedDescription)
                
                print("error")
            }
            
        }
    }
    
    class func getSubInterest(_ param:Dictionary<String,String>,resultCallback:@escaping ([SubInterest]) -> Void ,errorCallback:@escaping (_ err:String) -> Void){
        
        Alamofire.request(URLHelper.getURL("SUB_INTEREST"), method: .get, parameters: param, encoding: URLEncoding.default, headers: Constants.headerConstants([:])).responseJSON { (result) in
            if(result.result.isSuccess){
                let result = result.result.value as? Dictionary<String,AnyObject>
                if let _ = result?["developerMessage"] as? String{
                    errorCallback(result!["message"] as! String)
                }else{
                    if let interestJson = result?["subInterests"] as? [Dictionary<String,AnyObject>] {
                        let interests = interestJson.map({ (json) -> SubInterest in
                            return UserConverter.convertSubInterest(interstJson: json)
                        })
                        resultCallback(interests)
                    }
                }
                //resultCallback(true)
            }else{
                errorCallback(result.result.error?.localizedDescription ?? "")
                
                print("error")
            }
            
        }
    }
    
    class func findMentors(_ param:Dictionary<String,String>,resultCallback:@escaping ([User]) -> Void ,errorCallback:@escaping (_ err:String) -> Void){
        
        Alamofire.request(URLHelper.getURL("MENTORS"), method: .get, parameters: param, encoding: URLEncoding.default, headers: Constants.headerConstants([:])).responseJSON { (result) in
            if(result.result.isSuccess){
                let result = result.result.value as? Dictionary<String,AnyObject>
                if let _ = result?["developerMessage"] as? String{
                    errorCallback((result?["message"] as? String) ?? "")
                }else{
                    let mentorsJson = result?["mentors"] as? [Dictionary<String,AnyObject>]
                    let users = mentorsJson?.map({ (json) -> User in
                        return UserConverter.convert(userJson: json)
                    })
                    resultCallback(users!)
                }
                //resultCallback(true)
            }else{
                errorCallback(result.result.error!.localizedDescription)
                
                print("error")
            }
            
        }
    }
    
    
    class func getUser(param:Dictionary<String,String>,resultCallback:@escaping (User) -> Void ,errorCallback:@escaping (_ err:String) -> Void){
        
        Alamofire.request(URLHelper.getURL("SIGN_UP"), method: .get, parameters: param, encoding: URLEncoding.default, headers: Constants.headerConstants([:])).responseJSON { (result) in
            if(result.result.isSuccess){
                let result = result.result.value as? Dictionary<String,AnyObject>
                if let _ = result?["developerMessage"] as? String{
                    errorCallback((result?["message"] as? String) ?? "")
                }else{
                    print(result)
                    let user  = UserConverter.convert(userJson: result)
                    if let usr = self.user {
                        user.authToken = usr.authToken
                    }
                    let userData = NSKeyedArchiver.archivedData(withRootObject: user)
                    UserDefaults.standard.removeObject(forKey: "user")
                    UserDefaults.standard.set(userData, forKey: "user")
                    resultCallback(user)
                    
                }
                //resultCallback(true)
            }else{
                errorCallback(result.result.error!.localizedDescription)
                
                print("error")
            }
            
        }
    }
    
    
    class func getConnections(urlParam:Dictionary<String,String>,resultCallback:@escaping ([UserConnection]) -> Void ,errorCallback:@escaping (_ err:String) -> Void){
        
        Alamofire.request(URLHelper.getURL("MENTOR_CONNECTIONS",withURLParam:urlParam), method: .get, parameters: nil, encoding: URLEncoding.default, headers: Constants.headerConstants([:])).responseJSON { (result) in
            if(result.result.isSuccess) {
                let aresult = result.result.value as? Dictionary<String,AnyObject>
                if let _ = aresult?["developerMessage"] as? String{
                    errorCallback((aresult?["message"] as? String) ?? "")
                } else {
                    //   print(result)
                    if let connectionJson = aresult?["connections"] as? [Dictionary<String,AnyObject>] {
                        let connections = connectionJson.map({ (item) -> UserConnection in
                            UserConnectionConverter.convert(connectionJson: item)
                        })
                        resultCallback(connections)
                    } else {
                        errorCallback(result.result.error?.localizedDescription ?? "")
                    }
                }
                //resultCallback(true)
            }else{
                errorCallback(result.result.error?.localizedDescription ?? "")
            }
            
        }
    }
    
    class func acceptOrDeclineConnection(urlParam:String,resultCallback:@escaping (UserConnection) -> Void ,errorCallback:@escaping (_ err:String) -> Void){
        
        Alamofire.request("\(URLHelper.getURL("ACCEPT_OR_DECLINE_CONNECTIONS"))\(urlParam)", method: .put, parameters: nil, encoding: URLEncoding.default, headers: Constants.headerConstants([:])).responseJSON { (result) in
            if(result.result.isSuccess){
                let result = result.result.value as? Dictionary<String,AnyObject>
                if let _ = result?["developerMessage"] as? String{
                    errorCallback(result!["message"] as! String)
                }else{
                    
                    let connectionJson = result!
                    
                    let connection =  UserConnectionConverter.convert(connectionJson: connectionJson)
                    
                    resultCallback(connection)
                    
                }
                //resultCallback(true)
            }else{
                errorCallback(result.result.error!.localizedDescription)
                
                print("error")
            }
            
        }
    }
    class func establishConnection(urlParam:String,resultCallback:@escaping (Bool) -> Void ,errorCallback:@escaping (_ err:String) -> Void){
        
        Alamofire.request("\(URLHelper.getURL("ACCEPT_OR_DECLINE_CONNECTIONS"))\(urlParam)", method: .post, parameters: nil, encoding: URLEncoding.default, headers: Constants.headerConstants([:])).responseJSON { (result) in
            if(result.result.isSuccess){
                let result = result.result.value as? Dictionary<String,AnyObject>
                if let _ = result?["developerMessage"] as? String{
                    errorCallback((result?["message"] as? String) ?? "")
                } else {
                    resultCallback(true)
                }
                //resultCallback(true)
            }else{
                errorCallback(result.result.error!.localizedDescription)
                
                print("error")
            }
            
        }
    }
    
    class func rateUser(urlParam:String,resultCallback:@escaping (Bool) -> Void ,errorCallback:@escaping (_ err:String) -> Void){
        
        Alamofire.request("\(URLHelper.getURL("RATE_USER"))\(urlParam)", method: .post, parameters: nil, encoding: URLEncoding.default, headers: Constants.headerConstants([:])).responseJSON { (result) in
            let statusCode = result.response?.statusCode ?? 0
            if statusCode == 200 {
                let result = result.result.value as? Dictionary<String,AnyObject>
                if let _ = result?["developerMessage"] as? String{
                    errorCallback((result?["message"] as? String) ?? "")
                } else {
                    resultCallback(true)
                }
            } else {
                errorCallback(result.result.error?.localizedDescription ?? "")
            }
            
        }
    }
    
    enum BlockKey: String {
        case url = "/mentor/connection/block"
        case userEmail = "user-email"
        case blockEmail = "block-email"
        case connectionId = "connection-id"
    }
    class func blockUser(userEmail: String,
                         blockEmail: String,
                         connectionId: String,
                         resultCallback:@escaping (Bool) -> Void,
                         errorCallback:@escaping (_ err:String) -> Void) {
        let param: [String: Any] = [BlockKey.blockEmail.rawValue : blockEmail,
                                     BlockKey.userEmail.rawValue : userEmail,
                                     BlockKey.connectionId.rawValue : connectionId]
        
        Alamofire.request(Constants.baseURL() + BlockKey.url.rawValue + param.toURLEncode(),
                          method: .post,
                          parameters: param,
                          encoding: URLEncoding.default,
                          headers: Constants.headerConstants([:]))
            .responseJSON { (result) in
            if(result.response!.statusCode == 200) {
                let result = result.result.value as? Dictionary<String,AnyObject>
                if let message = result?[Key.message.rawValue] as? String {
                    errorCallback(message)
                }else{
                    resultCallback(true)
                }
            }else{
                errorCallback(result.result.error!.localizedDescription)
            }
        }
    }
    
    enum ReportKey: String {
        case url = "/mentor/connection/report"
        case userEmail = "user-email"
        case reportEmail = "report-email"
        case connectionId = "connection-id"
        case message = "messages"
    }
    class func reportUser(userEmail: String,
                         reportEmail: String,
                         connectionId: String,
                         messages: [String],
                         resultCallback:@escaping (Bool) -> Void,
                         errorCallback:@escaping (_ err:String) -> Void) {
        let param: [String: Any] = [ReportKey.reportEmail.rawValue : reportEmail,
                                    ReportKey.userEmail.rawValue : userEmail,
                                    ReportKey.connectionId.rawValue : connectionId,
                                    ReportKey.message.rawValue : messages.joined(separator: "<br>--------------------")]
        
        Alamofire.request(Constants.baseURL() + ReportKey.url.rawValue,
                          method: .post,
                          parameters: param,
                          encoding: URLEncoding.default,
                          headers: Constants.headerConstants([:]))
            .responseJSON { (result) in
                let statusCode = result.response?.statusCode ?? 0
                if statusCode == 200 {
                    let result = result.result.value as? Dictionary<String,AnyObject>
                    if let message = result?[Key.message.rawValue] as? String {
                        errorCallback(message)
                    }else{
                        resultCallback(true)
                    }
                }else{
                    errorCallback(result.result.error?.localizedDescription ?? "")
                }
        }
    }
    
    class func forgotPassword(urlParam:String,resultCallback:@escaping (Bool) -> Void ,errorCallback:@escaping (_ err:String) -> Void){
        
        Alamofire.request("\(URLHelper.getURL("FORGOT_PASSWORD"))\(urlParam)", method: .post, parameters: nil, encoding: URLEncoding.default, headers: [:]).responseJSON {(result) in
            if(result.result.isSuccess){
                let result = result.result.value as? Dictionary<String,AnyObject>
                if let _ = result?["developerMessage"] as? String {
                    errorCallback((result?["message"] as? String) ?? "")
                } else {
                    resultCallback(true)
                }
            } else {
                errorCallback(result.result.error?.localizedDescription ?? "")
            }
            
        }
    }
    
    class func sendPush(urlParam:String,resultCallback:@escaping (Bool) -> Void ,errorCallback:@escaping (_ err:String) -> Void){
        
        Alamofire.request("\(URLHelper.getURL("SEND_PUSH"))\(urlParam)", method: .post, parameters: nil, encoding: URLEncoding.default, headers: Constants.headerConstants([:])).responseJSON { (result) in
            if(result.result.isSuccess){
                let result = result.result.value as? Dictionary<String,AnyObject>
                if let _ = result?["developerMessage"] as? String{
                    errorCallback((result?["message"] as? String) ?? "")
                } else {
                    resultCallback(true)
                }
            }else{
                errorCallback(result.result.error?.localizedDescription ?? "")
            }
            
        }
    }
    
    
    class func updatePushToken(urlParam:String,resultCallback:@escaping (Dictionary<String,AnyObject>) -> Void ,errorCallback:@escaping (_ err:String) -> Void){
        
        Alamofire.request("\(URLHelper.getURL("USER_DEVICEPUSHTOKEN"))\(urlParam)", method: .post, parameters: nil, encoding: URLEncoding.default, headers: Constants.headerConstants([:])).responseJSON { (result) in
            if(result.result.isSuccess) {
                let result = result.result.value as? Dictionary<String,AnyObject>
                if let _ = result?["developerMessage"] as? String {
                    errorCallback((result?["message"] as? String) ?? "")
                }else {
                    resultCallback(result ?? [:])
                    
                }
            } else {
                errorCallback(result.result.error?.localizedDescription ?? "")
            }
        }
    }
}


