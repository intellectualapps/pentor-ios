//
//  ProfileNavViewController.swift
//  PentorApp
//
//  Created by Segun Solaja on 2/23/17.
//  Copyright © 2017 Pentor. All rights reserved.
//

import UIKit

class ProfileNavViewController: UINavigationController {
    
    var user: User? = User.archivedUser()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.configureView()
    }
    func configureView(){
        if let _ = user{
            let controller = self.storyboard?.instantiateViewController(withIdentifier: "basicInfo") as! BasicInfoViewController
            self.viewControllers[0] =  controller
        }else{
            let controller = self.storyboard?.instantiateViewController(withIdentifier: "profileController") as! ProfileViewController
            self.viewControllers[0] =  controller
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func configureMenuButton(_ menuButton: UIButton!) {
        menuButton.frame = CGRect(x: 0, y: 0, width: 40, height: 29);
        menuButton.setImage(UIImage(named:"menu-icon"), for: .normal)
    }
}
