//
//  User.swift
//  PentorApp
//
//  Created by Segun Solaja on 2/12/17.
//  Copyright © 2017 Pentor. All rights reserved.
//

import UIKit
//"yyyy-MM-dd'T'HH:mm:ss'Z'"
class User: NSObject,NSCoding {
    var authToken:String?
    var email:String?
    var gender:String?
    var createdOn:Date?
    var firstName:String?
    var lastName:String?
    var isMentor:Bool?
    var isEmailVerified:Bool?
    var longitude:Double?
    var latitude:Double?
    var longBio:String?
    var shortBio:String?
    var phoneNumber:String?
    var profilePhotoURL:String?
    var birthday:Date?
    var address:String?
    var subInterest : [SubInterest]?
    
    override init () {
        super.init()
    }
    
    func encode(with aCoder: NSCoder){
        aCoder.encode(authToken, forKey: "authToken")
        aCoder.encode(email, forKey: "email")
        aCoder.encode(createdOn, forKey: "createdOn")
        aCoder.encode(firstName, forKey: "firstName")
        aCoder.encode(lastName, forKey: "lastName")
        aCoder.encode(isMentor, forKey: "isMentor")
        aCoder.encode(isEmailVerified, forKey: "isEmailVerified")
        aCoder.encode(longitude, forKey: "longitude")
        aCoder.encode(latitude, forKey: "latitude")
        aCoder.encode(phoneNumber, forKey: "phoneNumber")
        aCoder.encode(longBio, forKey: "longBio")
        aCoder.encode(gender, forKey: "gender")
        aCoder.encode(shortBio, forKey: "shortBio")
        aCoder.encode(birthday, forKey: "birthday")
        aCoder.encode(address, forKey: "address")
        aCoder.encode(profilePhotoURL, forKey: "profilePhotoURL")
        aCoder.encode(subInterest, forKey: "subInterest")
    }
    required init(coder aDecoder: NSCoder){
        self.authToken = aDecoder.decodeObject(forKey: "authToken") as? String
        self.createdOn = aDecoder.decodeObject(forKey: "createdOn") as? Date
        self.email = aDecoder.decodeObject(forKey: "email") as? String
        self.firstName = aDecoder.decodeObject(forKey: "firstName") as? String
        self.lastName = aDecoder.decodeObject(forKey: "lastName") as? String
        self.isMentor = aDecoder.decodeObject(forKey: "isMentor") as? Bool
        self.isEmailVerified = aDecoder.decodeObject(forKey: "isEmailVerified") as? Bool
        self.longitude = aDecoder.decodeObject(forKey: "longitude") as? Double
        self.latitude = aDecoder.decodeObject(forKey: "latitude") as? Double
        self.phoneNumber = aDecoder.decodeObject(forKey: "phoneNumber") as? String
        self.longBio = aDecoder.decodeObject(forKey: "longBio") as? String
        self.gender = aDecoder.decodeObject(forKey: "gender") as? String
        self.shortBio = aDecoder.decodeObject(forKey: "shortBio") as? String
        self.birthday = aDecoder.decodeObject(forKey: "birthday") as? Date
        self.subInterest = aDecoder.decodeObject(forKey: "subInterest") as? [SubInterest]
        self.address = aDecoder.decodeObject(forKey: "address") as? String
        self.profilePhotoURL = aDecoder.decodeObject(forKey: "profilePhotoURL") as? String
        
        
    }

    func fullname() -> String {
        var name = ""
        if let fname = firstName {
            name = fname
        }
        if let lname = lastName {
            name += " " + lname
        }
        return name
    }

    //load defaul user
    class func archivedUser() -> User? {
        if let pData = UserDefaults.standard.object(forKey: "user") as? Data {
            if let user = NSKeyedUnarchiver.unarchiveObject(with: pData) as? User {
                return user
            }
        }
        return nil
    }
}
