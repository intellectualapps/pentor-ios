//
//  UIViewController+Ext.swift
//  PentorApp
//
//  Created by Nguyen Van Dung on 7/30/17.
//  Copyright © 2017 Pentor. All rights reserved.
//

import Foundation
import UIKit
import APESuperHUD

extension UIViewController {
    func showProgress() {
        APESuperHUD.removeHUD(animated: true, presentingView: self.view, completion: { _ in

        })
    }

    func hideProgress() {
        self.view?.isUserInteractionEnabled = true
        APESuperHUD.removeHUD(animated: true, presentingView: self.view, completion: { _ in

        })
    }
}
