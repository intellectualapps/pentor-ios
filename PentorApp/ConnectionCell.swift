//
//  ConnectionCell.swift
//  PentorApp
//
//  Created by Segun Solaja on 3/28/17.
//  Copyright © 2017 Pentor. All rights reserved.
//

import UIKit

class ConnectionCell: UITableViewCell {
    
    @IBOutlet weak var userImage: UIImageView!
    @IBOutlet weak var userFullName: UILabel!
    @IBOutlet weak var userBio: UILabel!
    @IBOutlet weak var userType: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.layoutIfNeeded()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
}
