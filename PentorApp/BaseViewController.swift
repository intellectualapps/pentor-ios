//
//  BaseViewController.swift
//  PentorApp
//
//  Created by Nguyen Van Dung on 7/30/17.
//  Copyright © 2017 Pentor. All rights reserved.
//

import Foundation
import UIKit

class BaseViewController: UIViewController {
    var viewDidAppeared = false
    deinit {
        print(NSStringFromClass(self.classForCoder) + "." + #function)
    }
}
