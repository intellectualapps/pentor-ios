//
//  SubInterest.swift
//  PentorApp
//
//  Created by Segun Solaja on 2/15/17.
//  Copyright © 2017 Pentor. All rights reserved.
//

import UIKit

class SubInterest: NSObject ,NSCoding {
    var id:String?
    var subId:String?
    var name:String?
    
    override init () {
        super.init()
    }
    
    func encode(with aCoder: NSCoder){
        aCoder.encode(id, forKey: "id")
        aCoder.encode(subId, forKey: "subId")
        aCoder.encode(name, forKey: "name")
    }
    
    required init(coder aDecoder: NSCoder){
        self.id = aDecoder.decodeObject(forKey: "id") as? String
        self.subId = aDecoder.decodeObject(forKey: "subId") as? String
        self.name = aDecoder.decodeObject(forKey: "name") as? String
    }
    
}
